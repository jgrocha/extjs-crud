<?php
include ("db.php");
$dados = $_POST['contactos'];
$registo = json_decode(stripslashes($dados), true);
// (Update) curl -X POST --form 'contactos={"id":"3","nome":"Jorge Gustavo Pereira Bastos Rocha","telefone":"910333888","email":"jgr@di.uminho.pt"}' http://localhost/~jgr/ExtJS4FirstLook/Code/Chapter%207/app/php/atualizaContacto.php
$id = $registo['id'];
unset($registo['id']);
$tabela = 'contacto';
$nfields = 0;
$update = "";
foreach ($registo as $key => $value) {
	$sep = ($nfields == 0) ? '' : ', ';
	switch (gettype($value)) {
		case "boolean" :
			$valor = $value ? "'1'" : "'0'";
			break;
		case "integer" :
		case "double" :
			$valor = $value;
			break;
		case "string" :
			$valor = "'" . $value . "'";
			break;
		case "NULL" :
			$valor = "NULL";
			break;
		default :
			$valor = "'" . $value . "'";
	}
	$update .= $sep . $key . "=" . $valor;
	$nfields += 1;
}
$sql = "update " . $tabela . " set " . $update . " where id = " . $id;
$affected =& $mdb2->exec($sql);
if (PEAR::isError($affected)) {
	$result["success"] = false;
	$result["errors"]["reason"] = $affected -> getMessage();
	$result["errors"]["query"] = $sql;
	die(json_encode($result));
} else {
	$linhasAfetadas = $affected;
	$result["total"] = $linhasAfetadas;
	if ($linhasAfetadas > 0) {
		$result["feedback"] = "O contacto '" . $id . "' foi alterado com sucesso.";
	} else {
		$result["feedback"] = "O contacto '" . $id . "' não foi alterado.";
	}
	$result["success"] = true;
	$result["sql"] = $sql;
	echo json_encode($result);
}
?>